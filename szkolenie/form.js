// (function(){
    
//   })();


/*
- getters
.css('background')
.attr('href')

- setters
.css('background', 'blue')
.attr('href', '/')


*/

jQuery(document).ready(function() {
    console.log($('img').height());

    $('img').on('load', function() {
        console.log($(this).height());
        console.log($(this).prop('naturalHeight'));
    })

    $('input').on('mouseenter', function() {
        $(this).css('background', 'red');
    })
    .on('mouseleave', function() {
        $(this).attr('style', null);
    });

    $('form').on('submit', function(event) {
        var valid = true;
        var form = $(this);

        form.find('input').each(function() {
            console.log(form.css('color'));

            if (!$(this).val()) {
                valid = false;
            }
        });

        if (!valid) {
            event.preventDefault();
            alert('bledne dane');
        }
    });

});