jQuery(document).ready(function() {
    var postsList = $('#posts-list');
    var modal = $('#modal').modal({
        show: false,
    });
    var posts = [];

    function renderPosts() {
        postsList.find('.entry').remove();

        posts.forEach(function(post) {
            var postHTML = $(`<tr class="entry">
                <td>${post.id}</td>
                <td>${post.title}</td>
                <td>${post.userId}</td>
            </tr>`);

            postHTML.data('post', post);

            postsList
                .find('tbody')
                .append(postHTML);
        });
    }

    postsList.on('click', 'thead th', function() {
        var th = $(this);
        var sortField = th.data('sort-field');
        var sortDir = th.data('sort-dir');

        th.data('sort-dir', sortDir === 'asc' ? 'desc' : 'asc');
        
        posts = posts.sort(function(a, b) {
            if (sortDir === 'asc') {
                return a[sortField] < b[sortField];
            } else {
                return a[sortField] > b[sortField];
            }
        });

        renderPosts();
    });

    postsList.on('click', '.entry', function() {
        var postData = $(this).data('post');
        
        modal.find('.modal-title').text(postData.title);
        modal.find('.modal-body').text(postData.body);

        modal.modal('show');
    });

    $.ajax({
        method: 'GET',
        url: 'https://jsonplaceholder.typicode.com/posts'
    })
        .done(function(data) {
            posts = data;
            
            renderPosts();
        }) // callback, kory wykona sie po sukcesie
        // .fail() // callback, ktory wykona sie gdy cos pojdzie nie tak
        // .always() // callback, wyona sie zawsze

        
        // postsList.find('.entry').on('click', function() { alert('test') });

        // == musi sie zgadzac wartosc, czyli 1 == '1' da true
        // === musi sie zgadzac wartosc i typ, czyli 1 === '1' da false
    
});