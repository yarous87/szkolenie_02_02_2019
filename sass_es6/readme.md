#Instrukcja uruchomienia
1. instalacja node.js (https://nodejs.org/en/) - najlepiej wersja LTS
2. w katalogu do którego sciągneliśmy projekt, instalujemy zależności poleceniem "npm i"
3. jesli chcemy skorzystać z wbudowanego serwera uruchamiamy polecenie "gulp serve", w przeciwnym wypadku "gulp watch"
4. w katalogach scss, es6 piszemy odpowiednio kod w sass'ie, ES6+ (ES6 - EcmaScript 6 - standard JS), pliki te są odpowiednio kompilowane do plików CSS (katalog css) i JS ES5 (katalog js)
5. w pliku html osadzamy pliki wynikowe (przekompilowane)

#Serwer jest dostępny pod adresem localhost:3000
