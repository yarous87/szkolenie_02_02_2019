const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const serve = require('gulp-serve');

gulp.task('sass', () => gulp.src(['app/scss/**.scss'])
    .pipe(plumber())
    .pipe(sass({
        includePaths: ['node_modules']
    }))
    .pipe(gulp.dest('./app/css'))
);

gulp.task('babel', () => gulp.src(['app/es6/**.js'])
    .pipe(plumber())
    .pipe(babel({
        presets: ['@babel/env']
    }))
    .pipe(gulp.dest('./app/js'))
)

gulp.task('watch', gulp.series(gulp.parallel('sass', 'babel'), () => {
    gulp.watch('app/scss/**/*.scss', gulp.series('sass'));
    gulp.watch(['app/es6/**.js'], gulp.series('babel'));
}));

gulp.task('serve', gulp.parallel('watch', serve('./app')));